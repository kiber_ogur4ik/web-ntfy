use serde::Deserialize;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::process::exit;
use std::{fs, thread, time::Duration};

#[derive(Deserialize)]
struct Config {
    ntfy_url: String,
    check_url: String,
    message: String,
    sleep: u64,
}

fn load_config() -> Config {
    let contents = match fs::read_to_string("config.toml") {
        Ok(c) => c,
        Err(_) => {
            eprintln!("File error");
            exit(1);
        }
    };
    let config: Config = match toml::from_str(&contents) {
        Ok(d) => d,
        Err(_) => {
            eprintln!("Unable to load config");
            exit(1);
        }
    };
    config
}

fn ntfy(message: &str) {
    minreq::post(load_config().ntfy_url)
        .with_body(message)
        .send()
        .unwrap();
}
fn get_url_hash(url: &str) -> u64 {
    let mut hasher = DefaultHasher::new();
    let response = minreq::get(url).send().unwrap();
    response.as_str().unwrap().hash(&mut hasher);
    hasher.finish()
}

fn main() {
    let config = load_config();
    //todo: make configurable in config.toml
    ntfy(
        format!(
            "Checking url {} with interval {} secs",
            config.check_url, config.sleep
        )
        .as_str(),
    );
    let mut hash_old = get_url_hash(config.check_url.as_str());
    let mut hash_current = get_url_hash(config.check_url.as_str());
    loop {
        if hash_current != hash_old {
            ntfy(config.message.as_str());
        }
        hash_old = hash_current;
        hash_current = get_url_hash(config.check_url.as_str());
        thread::sleep(Duration::from_secs(config.sleep));
    }
}
